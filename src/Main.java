import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";
    private static Scanner in;
    public static ArrayList<Integer>  historyGateways = new ArrayList<Integer>();


    public static void main(String []args){
        in = new Scanner(System.in);
        boolean repeat = true;

        enterOwnerData();
        while(repeat){
            enterData();
            validation();
            sendMessage();
            finish("");
            System.out.println("Do you wanna send another message? [y/n]");
            String vl = in.next();
            if (vl.equals("y")) repeat = true;
            else repeat = false;
        }
        Thread.currentThread().stop();
        // Like destructor
    }

    private static void enterOwnerData(){
        System.out.println("Hello, please tell me your name");
        Input.set("name",in.next());

        System.out.println(ANSI_GREEN+"You are welcome, "+Input.get("name")+ANSI_RESET);
        System.out.println("Enter your phone number to send message:");
        Input.set("phone",in.next());
    }

    private static void enterData(){

        System.out.println("Enter phone number of your friend:");
        Input.set("phone_to",in.next());

        System.out.println("Enter message:");
        Input.set("message",in.next());
    }

    private static void validation(){
        Validation validation = new Validation();

        boolean valid =  validation.validate(Input.get("phone_to"),Input.get("message"));
        if (!valid) {
            finish(ANSI_RED+validation.getErrors()+ ANSI_RESET);
            return;
        }
        Input.set("phone_to",validation.getPhone());
        Input.set("message",validation.getMessage());
    }

    private static void sendMessage(){
        Gateway gateway = null;

        while(gateway==null||historyGateways.size()<3){
            int randomGateway =  1 + (int) (Math.random() * 3);
            if (!historyGateways.isEmpty()){
                while (historyGateways.contains(randomGateway))
                    randomGateway =  1 + (int) (Math.random() * 3);
            }
            historyGateways.add(randomGateway);

            Output.set("Your random gateway: "+randomGateway);
            switch (randomGateway){
                case 1:
                    gateway = new NowSMS();
                    break;
                case 2:
                    gateway = new Diafaan();
                    break;
                case 3:
                    gateway = new Twillio();
                    break;
                default:
                    gateway = new NowSMS();
                    break;
            }

            gateway.connect(700,"host","user","pass");
            boolean send = gateway.send(700,"host","user","pass",Input.get("message"),Input.get("phone_to"));
            if (send){
                gateway.report(700,"host","user","pass");
                return;
            }
        }
    }

    private static void finish(String data){
        if (data!=null&&data.length()>0)
            Output.set(data);
        Output.show();
    }
}
