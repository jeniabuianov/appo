public class Twillio extends Gateway_Abstract{
    @Override
    public void connect(int port, String host, String user, String password) {
        boolean connect = Math.random() < 0.5;
        setConnection(connect);
    }

    @Override
    public boolean send(int port, String host, String user, String password, String message, String phone) {
        if (!isConnection()) {
            Output.set(Main.ANSI_RED + "Sorry. no connection.\n we cannot send message via Twillio" + Main.ANSI_RESET);
            return false;
        }
        Output.set("Phone from: "+Input.get("phone"));
        Output.set("Phone to: "+Input.get("phone_to"));
        Output.set("Message:");
        Output.set("Hi, it's "+Input.get("name")+". I'm testing Twillio. Did you get my message?");

        setMessage_id(String.valueOf(5 + (int) (Math.random() * 3500)));
        return true;

    }

    @Override
    public void report(int port, String host, String user, String password) {
        if (!isConnection()){
            Output.set(Main.ANSI_RED+"Sorry. no connection.\n we cannot get report\nSending gateway: Twillio"+Main.ANSI_RESET);
            return;
        }
        boolean randomGateway = (Math.random() < 0.5);
        if (randomGateway) Output.set("Message delivered, id: "+getMessage_id());
        else Output.set("Message is not delivered");
    }
}
