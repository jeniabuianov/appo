import java.util.*;

public abstract class Conditions_Phone extends Actions_Phone {
    private static HashMap<String,String> conditions;

    static {
        conditions = new HashMap<String,String>();
        conditions.put("hasMaxPhoneLength","A8");
        conditions.put("startNumber","B0");
        conditions.put("hasCode","C373");
        conditions.put("hasPlus","D+");

    }

    public static HashMap<String, String> getConditions() {
        return sortByValues(conditions);
    }


    public void setCondition(String condition, String value){
        conditions.put(condition,value);
    }

    public String getCondition(String condition){
        return conditions.get(condition);
    }

    protected boolean startNumber(String number, String value){
        return number.substring(0,value.length()).equals(value);
    }

    protected boolean hasCode(String number, String value){
        return startNumber(number,value);
    }

    protected boolean hasPlus(String number){
        return startNumber(number,"+");
    }

    protected boolean hasMaxPhoneLength(String number, int value){
        return number.length()>=value;
    }



    private static HashMap sortByValues(HashMap map) {
        List list = new LinkedList(map.entrySet());
        // Defined Custom Comparator here
        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o1)).getValue())
                        .compareTo(((Map.Entry) (o2)).getValue());
            }
        });

        // Here I am copying the sorted list in HashMap
        // using LinkedHashMap to preserve the insertion order
        HashMap sortedHashMap = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry) it.next();
            sortedHashMap.put(entry.getKey(), entry.getValue());
        }
        return sortedHashMap;
    }

}
