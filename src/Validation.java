import java.util.HashMap;

public class Validation extends Conditions_Phone {
    private String error;
    private String phone;
    private String message;

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPhone() {
        return phone;
    }

    public String getMessage() {
        return message;
    }

    private void setError(String error) {
        this.error = error;
    }

    public String getErrors() {
        return this.error;
    }

    public boolean validate(String phone, String message){
        HashMap<String,String> conditions = getConditions();
        for(String s:conditions.keySet()){
            String condition = conditions.get(s).substring(1);
            boolean next = false;
            switch (s) {
                case "hasMaxPhoneLength":
                    next = hasMaxPhoneLength(phone,Integer.parseInt(condition));
                    break;
                case "startNumber":
                    next = startNumber(phone,condition);
                    if (!next&&phone.length()>=8) next = true;
                    break;
                case "hasCode":
                    next = hasCode(phone,condition);
                    break;
                case "hasPlus":
                    next = hasPlus(phone);
                    break;
                default:
                    setError("Undefined method: "+s);
                    return false;
            }
            if (!next){
                setError("Cannot pass next.\nMethod: "+s+"\nPhone: "+phone+"\nMessage: "+message);
                return false;
            }
            String action = getAction(s);
            switch (action) {
                case "setCode":
                    phone = setCode(phone);
                    break;
                case "setPlus":
                    phone = setPlus(phone);
                    break;
                case "nxt":
                    break;
                case "error":
                    setError("Max length should be: "+condition);
                    break;
                default:
                    setError("Undefined method: "+action);
                    return false;
            }

        }
//        Thread.currentThread().stop();
        setMessage(message);
        setPhone(phone);
        return true;
    }

}
