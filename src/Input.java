import java.util.HashMap;

public class Input {
    private static HashMap<String,String> data = new HashMap<String, String>();

    public static void set(String key, String value) {
        if (data.get(key)!=null)
            data.replace(key,value);
        else
            data.put(key,value);
    }

    public static String get(String key){
        String value = data.get(key);
        if (value==null) return "";
        return value;
    }

}
