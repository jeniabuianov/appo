
public interface Gateway {

    void connect(int port, String host, String user, String password);
    boolean send(int port, String host, String user, String password, String message, String phone);
    void report(int port, String host, String user, String password);
}
