import java.util.HashMap;

public abstract class Actions_Phone {
    private static HashMap<String,String> actions;

    static {
        actions = new HashMap<String,String>();
        actions.put("hasMaxPhoneLength","error");
        actions.put("startNumber","setCode");
        actions.put("hasCode","setPlus");
        actions.put("hasPlus","nxt");
    }

    public static HashMap<String, String> getActions() {
        return actions;
    }

    public void setAction(String condition, String action){
        actions.put(condition,action);
    }

    public String getAction(String condition){
        return actions.get(condition);
    }

    protected String setCode(String number){
        if (number.length()>=10) return number;
        return number = "373"+number.substring(1);
    }

    protected String setPlus(String number){
        return number = "+"+number;
    }
}
