import java.util.ArrayList;

public class Output {
    private static ArrayList<String> data = new ArrayList<String>();

    public static ArrayList<String> getData() {
        return data;
    }

    public static void set(String value){
        data.add(value);
    }

    public static void show(){
        for (String s:data) {
            System.out.println(s);
        }
    }
}
